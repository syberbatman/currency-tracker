import requests
from config import API_ENDPOINT


def get_currency_rate(base='RUB', quotable='EUR'):
    response = requests.get('{0}/{1},{2}'.format(API_ENDPOINT, base, quotable))
    contents = response.json()
    return contents['data'][quotable][base]
